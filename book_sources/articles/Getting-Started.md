## Getting Started
  
Make sure you have the following prerequisites installed on your machine:

- TortoiseGit ([download](https://tortoisegit.org/))
<!-- Git ([download](https://git-scm.com/))-->
- nvm for Windows ([download](https://github.com/coreybutler/nvm-windows/releases/tag/1.1.8))